# sosasees sound library

sounds i recorded.

all sounds (and sound editing files, and photos) are licensed under
[Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/).

put this text when you use any sound or photo
from this sound library:

> sosasees sound library © 2023 by sosasees is licensed under
> [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/).

## sound editing files

after i record the sounds on my phone, i edit them in
[Tenacity](https://codeberg.org/tenacityteam/tenacity).
Tenacity project file names end with **.aup3**.
